﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoveController : MonoBehaviour{
    [Header("Movement")]
    public float moveAccel;
    public float currentAccel;
    public float currentSpeed;
    public float targetAccel;
    public float targetSpeed;
    public float maxSpeed;

    [Header("Jump")]
    public float jumpAccel;
    private bool isJumping;

    [Header("Dash")]
    public float dashSpeed;
    private bool isDashing;
    public float accelSmoothingTime;
    public float speedSmoothingTime;
    public bool waitAfterLanding;

    [Header("Ground Raycast")]
    public float groundRaycastDistance;
    public LayerMask groundLayerMask;
    private bool isOnGround;

    private Rigidbody2D rig;
    private Animator anim;
    private CharacterSoundController sound;

    [Header("Scoring")]
    public ScoreController score;
    public float scoringRatio;
    private float lastPositionX;

    [Header("GameOver")]
    public GameObject gameOverScreen;
    public float fallPositionY;

    [Header("Camera")]
    public CameraMoveController gameCamera;

    // Start is called before the first frame update
    void Start(){
        rig = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sound = GetComponent<CharacterSoundController>();
        targetSpeed = maxSpeed;
        targetAccel = moveAccel;
        waitAfterLanding = false;
    }

    // Update is called once per frame
    void Update(){
        // read jump input
        if (Input.GetMouseButtonDown(0)){
            if (isOnGround){
                isJumping = true;
                sound.PlayJump();
            }
        }

        // read dash input
        if (Input.GetMouseButtonDown(1)){
            if (isOnGround){
                isDashing = true;
                sound.PlayDash();
            }
        }
        if (Input.GetMouseButtonUp(1)){
            if (isOnGround){
                isDashing = false;
            }
            else{
                waitAfterLanding = true;
            }
        }
        if (isOnGround && waitAfterLanding){
            isDashing = false;
            waitAfterLanding = false;
        }


        // change animation
        anim.SetBool("isOnGround", isOnGround);

        // calculate score
        int distancePassed = Mathf.FloorToInt(transform.position.x - lastPositionX);
        int scoreIncrement = Mathf.FloorToInt(distancePassed / scoringRatio);

        if (scoreIncrement > 0)
        {
            score.IncreaseCurrentScore(scoreIncrement);
            lastPositionX += distancePassed;
        }

        // game over
        if (transform.position.y < fallPositionY)
        {
            GameOver();
        }
    }

    private void FixedUpdate(){
        // raycast ground
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, groundRaycastDistance, groundLayerMask);
        if (hit){
            if (!isOnGround && rig.velocity.y <= 0){
                isOnGround = true;
            }
        }
        else{
            isOnGround = false;
        }

        // move the character to the right at certain speed
        // calculate velocity vector
        Vector2 velocityVector = rig.velocity;

        if (isJumping){
            velocityVector.y += jumpAccel;
            isJumping = false;
        }

        velocityVector.x = Mathf.Clamp(velocityVector.x + currentAccel * Time.deltaTime, 0.0f, currentSpeed);
        rig.velocity = velocityVector;

        // smoothing current speed and current accel (due to dashing)
        currentAccel = Mathf.Lerp(currentAccel, targetAccel, accelSmoothingTime);
        currentSpeed = Mathf.Lerp(currentSpeed, targetSpeed, speedSmoothingTime);

        // dash handler
        if (isDashing){
            targetAccel = moveAccel * 10;
            targetSpeed = dashSpeed;
        }
        else{
            targetAccel = moveAccel;
            targetSpeed = maxSpeed;
        }
    }

    private void GameOver(){
        // set high score
        score.FinishScoring();

        // stop camera movement
        gameCamera.enabled = false;

        // show gameover
        gameOverScreen.SetActive(true);

        // disable this too
        this.enabled = false;
    }

    // ========== for debugging
    private void OnDrawGizmos(){
        Debug.DrawLine(transform.position, transform.position + (Vector3.down * groundRaycastDistance), Color.white);
    }
}
